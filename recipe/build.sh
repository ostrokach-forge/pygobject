#!/usr/bin/env bash

set -ev

export PYCAIRO_CFLAGS="${CFLAGS} -I${PREFIX}/include -I${PREFIX}/include/pycairo"
export PYCAIRO_LIBS="${LDFLAGS}"

# Dependencies have corrupt libtool files, leading to error:
# `../lib/libiconv.la' is not a valid libtool archive`
rm -f $PREFIX/lib/*.la

./configure --help
./configure --with-python=${PYTHON} --prefix="${PREFIX}"
make check TEST_NAMES=test_gi
make install
